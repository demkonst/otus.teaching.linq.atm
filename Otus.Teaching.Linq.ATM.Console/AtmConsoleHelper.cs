﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;

namespace Otus.Teaching.Linq.Atm
{
    public static class AtmConsoleHelper
    {
        public static User Login(ATMManager atmManager)
        {
            while (true)
            {
                System.Console.Write("Введите логин: ");
                var login = System.Console.ReadLine();

                System.Console.Write("Введите пароль: ");
                var password = System.Console.ReadLine();

                var user = atmManager.FindUser(login, password);
                if (user == null)
                {
                    System.Console.WriteLine("Пользователь не найден. Повторите попытку");
                    continue;
                }

                System.Console.WriteLine();
                return user;
            }
        }

        public static void PrintUser(User user)
        {
            System.Console.WriteLine("Пользователь:");
            System.Console.WriteLine(user.FullName);
            System.Console.WriteLine($"Телефон {user.Phone}, паспорт {user.PassportSeriesAndNumber}");
            System.Console.WriteLine($"Клиент банка с {user.RegistrationDate:d}");
            System.Console.WriteLine();
        }

        public static void PrintAccount(Account account)
        {
            System.Console.WriteLine($"{account.Id}. Открыт {account.OpeningDate:d}. Остаток {account.CashAll:F}");
        }

        public static void PrintOperation(OperationsHistory operation)
        {
            System.Console.WriteLine($"\t{operation.Id}. {operation.OperationType} {operation.CashSum:F} от {operation.OperationDate:d}");
        }

        public static void PrintOperation(OperationsHistory operation, User user)
        {
            System.Console.WriteLine($"{operation.Id}. {operation.CashSum:F} от {operation.OperationDate:d}; пользователь {user.FullName}");
        }
    }
}
