﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.Atm
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Старт приложения-банкомата...");
            Console.WriteLine();

            var atmManager = CreateAtmManager();

            var user = AtmConsoleHelper.Login(atmManager);
            AtmConsoleHelper.PrintUser(user);

            PrintAccounts(atmManager, user.Id, false);
            PrintAccounts(atmManager, user.Id, true);
            PrintCashInputs(atmManager);
            PrintFilthyRichUsers(atmManager);


            Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        private static ATMManager CreateAtmManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }

        private static void PrintAccounts(ATMManager atmManager, int userId, bool printOperations)
        {
            var accounts = atmManager.GetAccounts(userId);

            Dictionary<int, List<OperationsHistory>> operationsByAccountId = null;
            if (printOperations)
            {
                operationsByAccountId = atmManager.GetOperations(accounts.Select(x => x.Id))
                    .GroupBy(x => x.AccountId)
                    .ToDictionary(x => x.Key, x => x.ToList());
            }

            if (!accounts.Any())
            {
                Console.WriteLine("Счетов нет");
            }

            Console.WriteLine("Счета:");
            foreach (var account in accounts)
            {
                AtmConsoleHelper.PrintAccount(account);

                if (printOperations)
                {
                    if (!operationsByAccountId.TryGetValue(account.Id, out var operations))
                    {
                        Console.WriteLine("Операций нет");
                        continue;
                    }

                    foreach (var operation in operations)
                    {
                        AtmConsoleHelper.PrintOperation(operation);
                    }
                }
            }

            Console.WriteLine();
        }

        private static void PrintCashInputs(ATMManager atmManager)
        {
            var operations = atmManager.GetOperations(operationType: OperationType.InputCash);
            if (!operations.Any())
            {
                Console.WriteLine("Внесений наличных нет");
                return;
            }

            Console.WriteLine("Внесения наличных:");

            var accounts = atmManager
                .GetAccounts(accountIds: operations.Select(x => x.AccountId).Distinct())
                .ToDictionary(x => x.Id);

            var users = atmManager
                .GetUsers(accounts.Values.Select(x => x.UserId).Distinct())
                .ToDictionary(x => x.Id);

            foreach (var operation in operations)
            {
                var account = accounts[operation.AccountId];
                var user = users[account.UserId];
                AtmConsoleHelper.PrintOperation(operation, user);
            }

            Console.WriteLine();
        }

        private static void PrintFilthyRichUsers(ATMManager atmManager)
        {
            Console.Write("Показать пользователей, у которых сумма на счете больше ");
            var balanceGt = decimal.Parse(Console.ReadLine() ?? string.Empty);
            Console.WriteLine();

            var accounts = atmManager.GetAccounts(balanceGt: balanceGt);
            var users = atmManager.GetUsers(accounts.Select(x => x.UserId).Distinct());

            foreach (var user in users)
            {
                AtmConsoleHelper.PrintUser(user);
            }
        }
    }
}
