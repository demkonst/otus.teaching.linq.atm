﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public IEnumerable<Account> Accounts { get; }
        public IEnumerable<User> Users { get; }
        public IEnumerable<OperationsHistory> History { get; }

        public User FindUser(string login, string password)
        {
            return Users.FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        public List<User> GetUsers(IEnumerable<int> userIds = null)
        {
            var result = Users;

            if (userIds != null)
            {
                var ids = userIds.ToHashSet();
                result = result.Where(x => ids.Contains(x.Id));
            }

            return result.ToList();
        }

        public List<Account> GetAccounts(int? userId = null, IEnumerable<int> accountIds = null, decimal? balanceGt = null)
        {
            var result = Accounts;

            if (userId.HasValue)
            {
                result = result.Where(x => x.UserId == userId.Value);
            }

            if (accountIds != null)
            {
                var ids = accountIds.ToHashSet();
                result = result.Where(x => ids.Contains(x.Id));
            }

            if (balanceGt.HasValue)
            {
                result = result.Where(x => x.CashAll > balanceGt);
            }

            return result.ToList();
        }

        public List<OperationsHistory> GetOperations(IEnumerable<int> accountIds = null, OperationType? operationType = null)
        {
            var result = History;

            if (accountIds != null)
            {
                var ids = accountIds.ToHashSet();
                result = result.Where(x => ids.Contains(x.AccountId)).ToList();
            }

            if (operationType.HasValue)
            {
                result = result.Where(x => x.OperationType == operationType.Value);
            }

            return result.ToList();
        }
    }
}
